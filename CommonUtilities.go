
package jutilities

import (
	"fmt"
	"time"
)

// TODO: Come back and add a splitter
//// ParseDateFormat accepts a yyyyMMdd string with space, dot or hyphen breaks
//// formatString is the string of the format to be extracted
//// returns a standardised golang format
//func ParseDateFormat(formatString string) string {
//	if strings.Contains(formatString, ".")
//
//	strings.Split()
//
//}

// GetTimeDateFromUnixMillis
//
// returns standard dd-MM-yyy hh:mm:ss format string of unix millis passed
func GetTimeDateFromUnixMillis(unixMilli int64) string {
	timeNow := time.UnixMilli(unixMilli)
	return GetTimeDatePreset(&timeNow)
}

// GetTimeDate
//
// returns standard dd-MM-yyyy hh:mm:ss format string of time called
func GetTimeDate() string {
	timeNow := time.Now()
	return GetTimeDatePreset(&timeNow)
}

// GetTimeDatePreset
//
// timeNow is a time.Time object for parsing
//
// returns standard dd-MM-yyyy hh:mm:ss format string
func GetTimeDatePreset(timeNow *time.Time) string {
	if timeNow == nil {
		tempTimeNow := time.Now()
		timeNow = &tempTimeNow
	}

	return fmt.Sprintf(
		"%d-%d-%d %d:%d:%d",
		timeNow.Day(),
		timeNow.Month(),
		timeNow.Year(),
		timeNow.Hour(),
		timeNow.Minute(),
		timeNow.Second(),
	)
}

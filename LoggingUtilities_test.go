// Copyright Jack Roden (jroden2) 2022

package jutilities

import (
	"errors"
	"fmt"
	"github.com/rs/zerolog"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewLogger(t *testing.T) {
	tests := []struct {
		name string
	}{
		{
			"Validates creates a new Logger",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewLogger()
			assert.NotNil(t, got)
		})
	}
}

var (
	test = "Hello"
	logger = NewLogger()
)

type args struct {
	value  interface{}
	logger *zerolog.Logger
}

func TestLog(t *testing.T) {
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			"Test with string",
			args{
				value:  test,
				logger: nil,
			},
			"Hello",
		},
		{
			"Test with string",
			args{
				value:  test,
				logger: logger,
			},
			"Hello",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := Log(tt.args.value, tt.args.logger)
			assert.NotNil(t, got)
			fmt.Println(got)
		})
	}
}

func TestInfo(t *testing.T) {
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			"Test with string",
			args{
				value:  test,
				logger: nil,
			},
			"Hello",
		},
		{
			"Test with string",
			args{
				value:  test,
				logger: logger,
			},
			"Hello",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := Info(tt.args.value, tt.args.logger)
			assert.NotNil(t, got)
			fmt.Println(got)
		})
	}
}

func TestWarn(t *testing.T) {
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			"Test with string",
			args{
				value:  test,
				logger: nil,
			},
			"Hello",
		},
		{
			"Test with string",
			args{
				value:  test,
				logger: logger,
			},
			"Hello",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := Warn(tt.args.value, tt.args.logger)
			assert.NotNil(t, got)
			fmt.Println(got)
		})
	}
}

func TestError(t *testing.T) {
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			"Test with string",
			args{
				value:  test,
				logger: nil,
			},
			"Hello",
		},
		{
			"Test with string",
			args{
				value:  test,
				logger: logger,
			},
			"Hello",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := Error(tt.args.value, tt.args.logger)
			assert.NotNil(t, got)
			fmt.Println(got)
		})
	}
}

func TestErr(t *testing.T) {
	err := errors.New("Test error")

	tests := []struct {
		name string
		args args
		want string
	}{
		{
			"Test with err",
			args{
				value:  err,
				logger: nil,
			},
			"Hello",
		},
		{
			"Test with err",
			args{
				value:  err,
				logger: logger,
			},
			"Hello",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := Err(tt.args.value.(error), tt.args.logger)
			assert.NotNil(t, got)
			fmt.Println(got)
		})
	}
}
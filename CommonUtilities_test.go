// Copyright Jack Roden (jroden2) 2022

package jutilities

import (
	"fmt"
	"testing"
	"time"
)
import "github.com/stretchr/testify/assert"

var (
	timestamp = int64(1640898058279)
)

func TestGetTimeDateFromUnixMillis(t *testing.T) {

	tests := []struct {
		name string
		timeNow int64
	}{
		{
			"Check valid with set time",
			timestamp,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := GetTimeDateFromUnixMillis(tt.timeNow)
			fmt.Println(got)
			assert.NotEmpty(t, got)
		})
	}
}

func TestGetTimeDate(t *testing.T) {
	tests := []struct {
		name string
	}{
		{
			"Test date is correct format",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := GetTimeDate()
			fmt.Println(got)
			assert.NotEmpty(t, got)
		})
	}
}

func TestGetTimeDatePreset(t *testing.T) {
	setTime := time.UnixMilli(timestamp)

	tests := []struct {
		name string
		timeNow *time.Time
	}{
		{
			"Check valid with set time",
			&setTime,
		},
		{
			"Check returns with nil time set",
			nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := GetTimeDatePreset(tt.timeNow)
			fmt.Println(got)
			assert.NotEmpty(t, got)
		})
	}
}